package com.android.hola.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.android.hola.BinarApp
import com.android.hola.R
import com.android.hola.common.Constant
import com.android.hola.data.PreferenceHelper
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupView()

        testPref()

    }

    private fun cekUser() {
        val name :String = BinarApp.sp.nama
        if (name.isNotBlank()) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun testPref() {
        /*val sp:PreferenceHelper= BinarApp.sp*/

        //Java Style
        /*sp.putString(Constant.NAMA, "daniel")
        sp.putString(Constant.IG, "danielkurniawan__")
        sp.putString(Constant.EMAIL, "pacman1770@gmail.com")

        val nama:String = sp.getString(Constant.NAMA)
        val ig:String = sp.getString(Constant.IG)
        val email:String = sp.getString(Constant.EMAIL)*/

        /*Kotlin Style
                to Save Data*/
        /*sp.nama="Daniel"
        sp.ig="danielkurniawan__"
        sp.email="pacman1770@gmail.com"
*/
        /*to read or get data from SP, ex : ${sp.nama}*/
        /*println("Nama: ${sp.nama}")
        println("Instagram: ${sp.ig}")
        println("E-Mail: ${sp.email}")*/
    }

    private fun setupView() {
        btnLogin.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val username = etUsername.text.toString()
        val password = etPassword.text.toString()

        if (username== "Daniel" && password== "1") {
            //login sukses
            showMessage("Login Sukses!")
            BinarApp.sp.nama=username
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            //login gagal
           showMessage("Login Gagal! Username atau Password yang anda masukkan salah")
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
