package com.android.hola.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import com.android.hola.BinarApp
import com.android.hola.R
import com.android.hola.fragment.HomeFragment
import com.android.hola.fragment.MateriFragment
import com.android.hola.fragment.ProfilFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupView()
    }

    private fun setupView() {
        setFragment(HomeFragment.newInstance(), getString(R.string.home))
        bottomNav.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navHome -> setFragment(HomeFragment.newInstance(), getString(R.string.home))
                R.id.navMateri -> setFragment(MateriFragment.newInstance(), getString(R.string.materi))
                R.id.navProfil -> setFragment(ProfilFragment.newInstance(), getString(R.string.profil))
            }
            return@setOnNavigationItemSelectedListener true
        }
    }

    private fun setFragment(fragment: Fragment, title: String) {
        this.title = title
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commit()
    }
}
