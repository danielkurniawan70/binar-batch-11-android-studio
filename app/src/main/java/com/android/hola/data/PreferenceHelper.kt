package com.android.hola.data

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.android.hola.common.Constant

class PreferenceHelper(app: Application) {
    private val sp: SharedPreferences by lazy {
        app.getSharedPreferences("my_binar_app", Context.MODE_PRIVATE)
    }
    private val spe: SharedPreferences.Editor by lazy {
        sp.edit()
    }

    //Java Style for put and get data from sp
    fun putString(key: String, value: String){
        spe.putString(key, value).apply()
    }

    fun getString(key: String): String {
        return sp.getString(key, "") ?: ""
    }

    fun logOut() {
        spe.clear().apply()
    }
    //end Java Style

    //Kotlin Style
    var nama: String
        set(value) {
            spe.putString(Constant.NAMA, value).apply()
        }
        get() = sp.getString(Constant.NAMA, "") ?:""
    var ig: String
        set(value) {
            spe.putString(Constant.IG, value).apply()
        }
        get() = sp.getString(Constant.IG, "") ?:""
    var email: String
        set(value) {
            spe.putString(Constant.EMAIL, value).apply()
        }
        get() = sp.getString(Constant.EMAIL, "") ?:""
}