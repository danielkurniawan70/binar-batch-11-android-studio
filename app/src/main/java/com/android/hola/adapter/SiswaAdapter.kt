package com.android.hola.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.hola.R
import com.android.hola.model.Siswa
import kotlinx.android.synthetic.main.item_siswa.view.*

class SiswaAdapter(
    private val siswaList: List<Siswa>,
    private val onClick: (siswa: Siswa) -> Unit,
    private val onLongClick: (siswa: Siswa) -> Unit
) : RecyclerView.Adapter<SiswaAdapter.Holder>() {

    override fun onCreateViewHolder(group: ViewGroup, type: Int): Holder {
        val layoutInflater = LayoutInflater.from(group.context)
        val view: View = layoutInflater.inflate(R.layout.item_siswa, group, false)
        return Holder(view)
    }

    override fun getItemCount(): Int = siswaList.size

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val siswa = siswaList[position]
        holder.bind(siswa)
        holder.itemView.run {
            setOnClickListener{
                onClick(siswa)
            }
            setOnLongClickListener {
                onLongClick(siswa)
                return@setOnLongClickListener false
            }
        }
    }

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(siswa: Siswa) = itemView.run {
            tvStudentName.text = siswa.nama
            tvStudentEmail.text = siswa.email
        }
    }

}