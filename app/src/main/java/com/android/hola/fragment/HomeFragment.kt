package com.android.hola.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.android.hola.R
import com.android.hola.adapter.SiswaAdapter
import com.android.hola.common.toast
import com.android.hola.model.Siswa
import kotlinx.android.synthetic.main.fragment_home.view.*


class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private val siswaList = mutableListOf<Siswa>()
    private val siswaAdapter = SiswaAdapter(siswaList, this::onItemClick, this::onLongClick)

    private fun onLongClick(siswa: Siswa) {
        toast("Long Click : ${siswa.nama}")
    }

    private fun onItemClick(siswa: Siswa) {
        toast("Click : ${siswa.nama}")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
        getSiswa()
    }

    private fun setupView() = view?.run {
        rvSiswa?.layoutManager = LinearLayoutManager(context)
        rvSiswa?.adapter = siswaAdapter
    }

    private fun getSiswa() {
        (1..15).forEach { nomor ->
            val siswa = Siswa("Student $nomor", "siswa$nomor@gmail.com")
            siswaList.add(siswa)
        }
        siswaAdapter.notifyDataSetChanged()
    }

}
